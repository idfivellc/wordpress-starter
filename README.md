# WordPress Starter

Use this repository to start a new project that uses the idfive Component Library. 

## Getting started

 1. Download this as a .zip and delete any .git folders before starting. 
 1. The theme included here is a copy of the [Timber Starter Theme](https://github.com/timber/starter-theme). This theme will be updated regularly as part of the maintenance of this project.
 1. Rename the theme folder to an appropriate name, based on the project. 
 1. Edit style.css inside the theme folder and add the proper theme details. 

### Set Up WordPress

 1. Duplicate `wp-content/snippets/wp-config-sample.php` into the WordPress root folder and rename it to `wp-config.php`. This version includes a conditional statement that loads `wp-config-local.php` if it is present.
 1. Set up a local WordPress installation. Duplicate `wp-content/snippets/wp-config-local-sample.php` into the WordPress root folder and rename it to `wp-config-local.php`.
 1. Edit `wp-config-local.php` as needed. 
 
### Component Library

 1. Download a .zip file of the [idfive component library](https://bitbucket.org/idfivellc/idfive-component-library/src/master/). Do not delete the `.gitignore` file in this repository.
 1. Rename the downloaded package to `idfive-component-library` and move it to the theme folder. 
 1. Commit this entire folder. It is now forked independently of the main ICL project.
 1. The front-end developer will work from inside the above ICL folder.
 1. The included theme in this repository includes enqueue references for the build directory. This requires that the folder name for the front-end project stays as `idfive-component-library`.
 
### Timber and ACF

 1. The theme folder in this repository includes an acf-json folder.
 1. Activate ACF Pro plugin.
 1. Activate Timber plugin.
 1. Activate the theme.
 1. Browse to the ACF settings page
 1. Synchronize the existing field groups. 
 1. Options Page: a default options page is part of the theme. Option values are made available site-wide. ([More info](https://timber.github.io/docs/guides/acf-cookbook/#get-all-info-from-your-options-page).)
 
### Notes on Included  Plugins
 
 1. `Prevent XSS Vulnerability`: exclude `[` and `]` from the `Block Entities (Remove Entities)` field. This ensures that API requests are respected.
 1. `Search Everything`: allows custom fields to be searchable. 
 